#include <iostream>
#include <vector>
#include <type_traits>

class FlatPrinter
{
public:
    template<typename T>
    void operator()(T&& t)
    {
        std::cout << t << " ";
    }
};

template<typename FlatVisitor>
class Visitor
{
public:
    template<typename T>
    using is_class = std::is_class<std::remove_pointer_t<std::remove_reference_t<T>>>;

    template<typename T>
    using is_pointer = std::is_pointer<std::remove_reference_t<T>>;

    template<typename T>
    std::enable_if_t<!is_class<T>::value, void> operator&(T&& t)
    {
        m_flat_visitor(std::forward<T>(t));
    }

    template<typename T>
    std::enable_if_t<is_class<T>::value, void> operator&(T&& t)
    {
        apply_for_class(std::forward<T>(t));
    }

    template<typename T>
    std::enable_if_t<!is_pointer<T>::value, void> apply_for_class(T&& t)
    {
        t.serialize(*this);
    }

    template<typename T>
    std::enable_if_t<is_pointer<T>::value, void> apply_for_class(T&& t)
    {
        t->serialize(*this);
    }

private:
    FlatVisitor m_flat_visitor;
};

struct SimpleTestType
{
    int64_t i64 = 0;
    uint32_t ui32 = 0;

    template<class Archive>
    void serialize(Archive & ar)
    {
        ar & i64;
        ar & ui32;
    }
};

struct TestType
{
    int32_t i32 = 0;
    SimpleTestType simple_test_val;
    SimpleTestType* p_simple_test_val = &simple_test_val;

    template<class Archive>
    void serialize(Archive & ar)
    {
        ar & i32;
        ar & simple_test_val;
        ar & p_simple_test_val;
    }
};

int main(void)
{
    TestType in = { 0, { 42, 146 } };
    Visitor<FlatPrinter> ar;
    ar & in;
    return 0;
}
